import { Injectable } from '@angular/core';
import { Flight } from './flight';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({providedIn: 'root'})
export class FlightsService {

  flights: Flight[];

  constructor(private http: HttpClient) { }

  public getFlights(): Observable<any> {
    return this.http.get('http://localhost:8080/flights/');
  }

  public saveFlight(flight: Flight) {
    this.http.post('http://localhost:8080/flights/', flight).subscribe( data => {});

  }

  public delFlight(id: number) {

  }

}

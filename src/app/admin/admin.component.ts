import { Component, OnInit } from '@angular/core';
import { FlightsService } from '../flights.service';
import { Flight } from '../flight';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  public home: string;
  public end: string;

  constructor(private flightService: FlightsService) {
  }

  ngOnInit(): void {

  }

  public addFlight() {
    const startD: Date = new Date('2020-07-03T14:54:24.069+00:00');
    const endD: Date = new Date('2020-07-05T15:54:24.069+00:00');
    const flight: Flight = {
      
      home: this.home,
      destination: this.end,
      depart: startD,
      arrival: endD,
      isNonStop: true
    };
    this.flightService.saveFlight(flight);
    console.log("addFrlight metod stert");
  }



}

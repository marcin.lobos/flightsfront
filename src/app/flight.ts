

export interface Flight {
    id?: number;
    home: string;
    destination: string;
    depart: Date;
    arrival: Date;
    isNonStop: boolean;
}
